import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.delay(10)

WebUI.click(findTestObject('Pesanan/menu_daftarpesanan'), FailureHandling.STOP_ON_FAILURE)

WebUI.setText(findTestObject('Pesanan/field_cariinv'), '1617682778042')

WebUI.click(findTestObject('Pesanan/card_pesanan'))

WebUI.delay(5)

WebUI.click(findTestObject('Pesanan/btn_ubahstatus'))

WebUI.click(findTestObject('Pesanan/btn_closeX'))

WebUI.delay(10)

WebUI.click(findTestObject('Pesanan/menu_daftarpesanan'))

WebUI.delay(5)

WebUI.click(findTestObject('Pesanan/menu_dalamproses'))

WebUI.click(findTestObject('Pesanan/menu_selesai'))

WebUI.click(findTestObject('Pesanan/menu_menunggu'))

